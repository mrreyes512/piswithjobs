# Pis With Jobs

My confession: I am a pi horder. I have 8 Raspberry Pis around my house. 

**My Inventory**    

| Model     | Quantity | 
| --------- | -------- |
| RPi 3 B+  | 4 | 
| RPi 3     | 2 | 

## Pi Jobs

### Home Assistant

An awesome home automation project that provides a single pane of glass to all of my gagets around the house

### Magic Mirror

A mirror that provides information and things

### Pi Hole

A network wide DNS filter that blocks common ad links

### Traffic Pi

A Python script that runs a traffic hat that provides red, yellow, green stats based on an API call

### Icinga Pi

A nice UI for network monitoring